# Install utilities for NvCert
- These tasks are intended to be run on the x86-host.
- They install the packages needed for Nvidia certification.
- Set systcl arp_ignore=1 to ensure traffic stays on the link with the specific IP address
