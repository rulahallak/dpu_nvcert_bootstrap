###############################################################################
#
# Copyright 2023 NVIDIA Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
###############################################################################


###############################################################################
# Deploys the DOCA HBN (Host Based Networking) service container on the DPU.
# DOCA HBN provides classic top of rack (ToR) routing/switching/network
# overlay capabilities on the DPU for the host.

# If you have more than one DPU on the host you can specify the `rshim_num`
# (default rshim_num is 0) for e.g. to install the HBN container on the second
# DPU hosted by the x86 you can specify `-e rshim_num=1`.
###############################################################################


- hosts: dpus
  become: true
  gather_facts: true
  tasks:
    - name: Install unzip
      apt:
        name: unzip
        state: present

    - name: Set local container path
      set_fact:
        local_dir: "~/nvcert/hbn"

    - name: Remove any existing dir
      file:
        path: "{{ local_dir }}"
        state: absent

    - name: Create a working dir
      file:
        path: "{{ local_dir }}"
        state: directory
        owner: ubuntu
        group: ubuntu
        mode: 0755
        recurse: yes

- hosts: dpus
  become: true
  gather_facts: true
  # If ngc_api_key is defined download the doca container config from
  # the private registry and move it to ~/doca_container
  tasks:
    - name: Download container config from the private NGC registry
      block:
        - name: Set ngc_org to nvstaging
          set_fact:
            ngc_org: "nvstaging"
          when: ngc_org is not defined

        - name: Set ngc_team to doca
          set_fact:
            ngc_team: "doca"
          when: ngc_team is not defined

        - name: Set container config path
          set_fact:
            doca_container_hbn_config: "{{ doca_dev_container_hbn_config }}"

        - name: Install NGC CLI
          include_role:
            name: nc-install-ngc-cli

        - name: Setup NGC config with ngc_api_key
          include_role:
            name: nc-ngc-config

        - name: Download container config from NGC
          command:
            cmd: ngc registry resource download-version "{{ ngc_org }}/{{ ngc_team }}/doca_container_configs" --dest "{{ local_dir }}"
          register: container_config_download

        - name: Get local container path
          command: /usr/bin/env python3
          args:
            stdin: |
                import json
                local_path = {{ container_config_download.stdout.strip() }}.get("local_path")
                print(local_path)
          register: local_path_str

        - name: Set local container path
          set_fact:
            local_path: "{{ local_path_str.stdout.strip()}}"

        - name: Run hbn-dpu-setup script
          shell: "cd {{ local_path }}/{{ doca_dev_container_hbn_script_path }} && chmod +x ./hbn-dpu-setup.sh && ./hbn-dpu-setup.sh" # noqa 305

      when: (ngc_api_key is defined)

- hosts: dpus
  become: true
  gather_facts: true
  # if ngc_api_key is not defined download the release doca container config
  # and move it to ~/doca_container
  tasks:
    - name: Download release container config
      block:
        - name: Download DOCA container configs
          ansible.builtin.get_url:
            validate_certs: false
            url: "{{ doca_release_container_url }}"
            dest: "/tmp/doca_container_configs.zip"
            force: true

        - name: Unzip DOCA container configs
          shell: "unzip -o /tmp/doca_container_configs.zip -d ~/nvcert/doca_container"

        - name: Set local container path
          set_fact:
            local_path: "~/nvcert/doca_container"

        - name: Set container config path
          set_fact:
            doca_container_hbn_config: "{{ doca_release_container_hbn_config }}"

        - name: Run hbn-dpu-setup script for hbn-verion
          shell: "cd {{ local_path }}/{{ doca_release_container_hbn_script_path }} && chmod +x ./hbn-dpu-setup.sh && ./hbn-dpu-setup.sh" # noqa 305

      when: (ngc_api_key is not defined)

# Powercycle the host after HBN setup (DPU reboot doesn't seem to be sufficient. why?)
- hosts: x86_hosts
  become: yes
  roles:
    # Power cycle the host after BFB install and link type change
    - role: nc-reboot-host

- hosts: dpus
  become: true
  gather_facts: false
  pre_tasks:
    - name: Wait for the DPU to come up after the server reboot
      include_role:
        name: nc-check-device-up

    - name: Start HBN container by copying doca_hbn.yml to kubelet.d
      ansible.builtin.copy:
        src: "{{ local_path }}/{{ doca_container_hbn_config }}"
        dest: "/etc/kubelet.d/"
        remote_src: true

    - name: Wait 2 minutes for HBN container to start
      pause:
        minutes: 2

  roles:
    - role: nc-get-hbn-state

  post_tasks:
    - name: Print hbn container state
      debug:
        msg: "doca-hbn is {{ hbn_state }} on dpu-{{ ansible_host }}"
