###############################################################################
#
# Copyright 2023 NVIDIA Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
###############################################################################


# This playbook configures the DPU to be in NIC mode

- hosts: dpu_bmcs
  become: yes
  gather_facts: no
  tasks:
    - name: Change the first time BMC password (0penBmc)
      import_role:
        name: nc-config-dpu-bmc-password
      delegate_to: localhost

    - name: Role - Start mst
      import_role:
        name: nc-start-mst
      delegate_to: x86

    # Get the mst device id
    - name: Role - Get mst name
      import_role:
        name: nc-get-dpu-mst-dev-id
      delegate_to: dpu

    - name: Role - Get DPU mode
      import_role:
        name: nc-get-dpu-mode
      delegate_to: x86

    - name: Save ansible_host variable to bmc_ip
      set_fact:
        bmc_ip: "{{ ansible_host }}"

    - name: Save ansible_password variable to bmc_password
      set_fact:
        bmc_password: "{{ ansible_password }}"

    - name: Set DPU to be in NIC mode
      shell: "curl -k -u root:{{ bmc_password }} -H 'Content-Type:application/json' -X POST -d '{\"Mode\":\"NicMode\"}' https://{{ bmc_ip }}/redfish/v1/Systems/Bluefield/Oem/Nvidia/Actions/Mode.Set"
      when: (dpu_mode is defined) and (dpu_mode == "ECPF")
      delegate_to: localhost


# Power cycle the host for new configurations to take place
- hosts: x86_hosts
  become: yes
  roles:
    - role: nc-reboot-host
